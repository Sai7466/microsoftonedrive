import express from 'express';
import axios from 'axios';
import * as msal from '@azure/msal-node';
import FormData from 'form-data';
import fs from 'fs';


import dotenv from 'dotenv';
dotenv.config();

const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.json());

// MSAL configuration
const cca = new msal.ConfidentialClientApplication({
  auth: {
    clientId: process.env.CLIENT_ID,
    authority: process.env.AUTHORITY,
    clientSecret: process.env.CLIENT_SECRET,
    redirectUri: process.env.REDIRECT_URI,
  },
});

// OneDrive API endpoints
//const graphBaseUrl = 'https://graph.microsoft.com/v1.0/me/drive/root:/';

// Store access token in-memory
let accessToken;

// Middleware to handle access tokens
const handleAccessToken = async (req, res, next) => {
  try {
    // Check if access token exists or expired
    if (!accessToken) {
      console.log("there is access token ,it is expired")
    }

    // Attach the access token to the request object
    req.accessToken = accessToken;

    // Continue with the next middleware or route handler
    next();
  } catch (error) {
    console.error('Error handling access token:', error);
    res.status(500).send('Internal Server Error');
  }
};

// Middleware to log requests
const requestLogger = (req, res, next) => {
  console.log(`Received ${req.method} request for ${req.url}`);
  next();
};

// Apply middleware
app.use(handleAccessToken);
app.use(requestLogger);

// Routes

// Authentication callback
app.get('/auth-callback', async (req, res) => {
  try {
    const authorizationCode = req.query.code;

    if (!authorizationCode) {
      throw new Error('Authorization code not found in the query parameters');
    }

    accessToken = await getOneDriveToken(authorizationCode);

    res.redirect('/success');
  } catch (error) {
    console.error('Error handling callback:', error);
    res.status(500).send('Internal Server Error');
  }
});

// Get OneDrive data
app.get('/api/getOneDriveData', async (req, res) => {
    try {
        const response = await axios.get('https://graph.microsoft.com/v1.0/me/drive/root/children', {
            headers: {
                Authorization: `Bearer ${accessToken}`,
            },
        });

        res.status(200).json({message:"successfully get the data",data:response.data});
        console.log('OneDrive getting Response:', response.data);
    } catch (error) {
        // Handle error
        console.error('OneDrive Data Error:', error.response, error.response.data);
        res.status(error.response.status).json({ error: 'Error fetching OneDrive data' });
    }
});


// Upload to OneDrive
app.post('/api/uploadToOneDrive', async (req, res) => {
    try {
        const fileStream = fs.createReadStream('Evaluationform.pdf');
        const form = new FormData();
        form.append('file', fileStream);

        const response = await axios.put('https://graph.microsoft.com/v1.0/me/drive/root:/Evaluationform.pdf:/content', form, {
            headers: {
                Authorization: `Bearer ${accessToken}`,
                ...form.getHeaders(),
            },
        });

        console.log('OneDrive Upload Response:', response.data);
        res.status(200).json({message:"successfully uploaded",data:response.data});
    } catch (error) {r
        console.error('Upload to OneDrive Error:', error.response.status, error.response.data);
        res.status(error.response.status).json({ error: 'Error uploading to OneDrive' });
    }
});


// Delete file from OneDrive
app.delete('/api/deleteFile/:fileId', async (req, res) => {
    try {
        // const fileId = 'ET0Q0fucW7dDgM9bumqAkzEBH0O4bY5rgXai7dyuO9eehAw'; // Replace with the actual file ID
        const fileId = req.params.fileId;
        const response = await axios.delete(`https://graph.microsoft.com/v1.0/me/drive/items/${fileId}`, {
            headers: {
                Authorization: `Bearer ${accessToken}`,
            },
        });

        console.log('OneDrive Delete Response:', response.data);
        res.status(200).json({message:"successfully deleted",data:response.data});
    } catch (error) {
        console.error('Delete File Error:', error.response.status, error.response.data);
        res.status(error.response.status).json({ error: 'Error deleting file from OneDrive' });
    }
});

app.put('/api/updateFile/:fileId', async (req,res)=>{
  try{
    const fileName= req.params.fileId;
    const updateData = {
      name: 'UpdatedFileName.docx',
      
    };
    const response=await axios.patch(`https://graph.microsoft.com/v1.0/me/drive/items/${fileName}`,updateData,{
      headers:{
        Authorization: `Bearer ${accessToken}`,
        'Content-Type': 'application/json'
      }
    });
    console.log('OneDrive Delete Response:', response.data)
    res.status(200).json({message:"successfully updated",data:response.data});

  }
  catch (error) {
    console.error('update  File Error:', error.response.status, error.response.data);
    res.status(error.response.status).json({ error: 'Error updating file from OneDrive' });
}
})

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

// Helper function to get OneDrive access token
async function getOneDriveToken(authorizationCode) {
  try {
    const tokenRequest = {
      code: authorizationCode,
      redirectUri: process.env.REDIRECT_URI,
      scopes: ['User.Read', 'Files.ReadWrite','Sites.Read','Sites.ReadWrite'],
    };

    console.log('Requesting token with authorization code:', tokenRequest);

    const result = await cca.acquireTokenByCode(tokenRequest);

    console.log('Token response:', result);

    if (result && result.accessToken) {
      console.log('Access Token:', result.accessToken);
      return result.accessToken;
    } else {
      throw new Error('Access token not found in the response');
    }
  } catch (error) {
    console.error('Error getting OneDrive token:', error);
    throw error;
  }
}

